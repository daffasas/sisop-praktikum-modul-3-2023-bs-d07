#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MAX_MSG_SIZE 1024

typedef struct msg_buffer {
    long msg_type;
    char msg_text[MAX_MSG_SIZE];
} MsgBuffer;

int main() {
    key_t key = ftok("msgq_file", 'A');
    int msg_id = msgget(key, 0666 | IPC_CREAT);
    MsgBuffer msg_buffer;
    msg_buffer.msg_type = 1;

    while (1) {
        printf("Enter message to send: ");
        fgets(msg_buffer.msg_text, MAX_MSG_SIZE, stdin);
        msgsnd(msg_id, &msg_buffer, sizeof(MsgBuffer), 0);
        if (strcmp(msg_buffer.msg_text, "quit\n") == 0) {
            break;
        }
    }

    printf("Message queue deleted.\n");
    return 0;
}
